<?php
require_once 'autoload.php';

use Actions\ActionInterface;
use Actions\Division;
use Actions\Minus;
use Actions\Multiply;
use Actions\Plus;
use Log\FileLogger;

//Define options
$consoleOptions = array(
    'a:'    => 'action:',
    'f:'    => 'file:',
    'l:'    => 'log:'
);

$options = getopt( implode('', array_keys($consoleOptions)), $consoleOptions );

$action = $options['a'] ?? $options['action'] ?? null;

$file = $options['f'] ?? $options['file'] ?? 'notexists.csv';
$inputPath = __DIR__ . '/' . $file;
$outputPath = __DIR__ . '/result.csv';

$logFile = $options['l'] ?? $options['log'] ?? 'test.log';

if (empty($options)) {
    Output::show(Help::get());
    exit;
}

$logger = new FileLogger(__DIR__ . '/' . $logFile);

try {
    switch ($action) {
        case 'plus':
            $actionClass = Plus::class;
            break;
        case 'minus':
            $actionClass = Minus::class;
            break;
        case 'multiply':
            $actionClass = Multiply::class;
            break;
        case 'division':
            $actionClass = Division::class;
            break;
        default:
            throw new InvalidArgumentException("Wrong action is selected");
    }

    /** @var ActionInterface $executable */
    $executable = new $actionClass($inputPath, $outputPath, $logger);
    $executable->execute();

} catch (Exception $exception) {
    Output::show($exception->getMessage(), Output::ERROR);
    exit(1);
}

Output::show('Done', Output::SUCCESS);
exit;