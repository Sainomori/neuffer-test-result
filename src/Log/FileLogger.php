<?php
namespace Log;

use DateTime;
use Exception;

class FileLogger implements LoggerInterface
{
    /** @var string  */
    private $logFilePath;
    /** @var string  */
    public $template = "{date} {level} {message} {context}";
    /** @var string  */
    public $dateFormat = DateTime::RFC2822;

    /**
     * Logger constructor.
     * @param string $logFilePath
     */
    public function __construct(string $logFilePath = '')
    {
        $this->logFilePath = $logFilePath;
        if (file_exists($this->logFilePath)) {
            unlink($this->logFilePath);
        }
        touch($this->logFilePath);
    }

    /**
     * PSR-like log method
     *
     * @param string $level
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function log($level, $message, array $context = [])
    {
        //Well... It could be slow. Maybe I should open file descriptor at constructor and use existing file link for writing.
        file_put_contents($this->logFilePath, trim(strtr($this->template, [
                '{date}' => $this->getDate(),
                '{level}' => $level,
                '{message}' => $message,
                '{context}' => $this->getContextJSON($context),
            ])) . PHP_EOL, FILE_APPEND);
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws Exception
     */
    public function emergency($message, array $context = array())
    {
        $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws Exception
     */
    public function alert($message, array $context = array())
    {
        $this->log(LogLevel::ALERT, $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws Exception
     */
    public function critical($message, array $context = array())
    {
        $this->log(LogLevel::CRITICAL, $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws Exception
     */
    public function error($message, array $context = array())
    {
        $this->log(LogLevel::ERROR, $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws Exception
     */
    public function warning($message, array $context = array())
    {
        $this->log(LogLevel::WARNING, $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws Exception
     */
    public function notice($message, array $context = array())
    {
        $this->log(LogLevel::NOTICE, $message, $context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws Exception
     */
    public function info($message, array $context = array())
    {
        $this->log(LogLevel::INFO, $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     * @throws Exception
     */
    public function debug($message, array $context = array())
    {
        $this->log(LogLevel::DEBUG, $message, $context);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getDate() :string
    {
        return (new DateTime())->format($this->dateFormat);
    }

    /**
     * @param array $context
     * @return string|null
     */
    public function getContextJSON(array $context = []) :?string
    {
        return !empty($context) ? json_encode($context) : null;
    }
}
