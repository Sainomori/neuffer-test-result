<?php
namespace Actions;

class Division extends AbstractAction
{

    /**
     * Should check is these variables could be processed
     * @param $a
     * @param $b
     * @return bool
     */
    protected function validate($a, $b): bool
    {
        if ($a < 0 || $b < 0) return false;
        if ($b == 0) return false;
        return true;
    }

    /**
     * Should return result of operation
     * @param $a
     * @param $b
     * @return mixed
     */
    protected function getResult($a, $b)
    {
        return intval($a) / intval($b);
    }

    /**
     * Should return operation name for logging purposes
     * @return string
     */
    protected function getOperationName(): string
    {
        return 'division';
    }
}