<?php


namespace Actions;

use Exception;
use Log\LoggerInterface;
use Log\LoggerTrait;

abstract class AbstractAction implements ActionInterface
{
    use LoggerTrait;

    /** @var LoggerInterface  */
    protected $logger;
    /** @var string  */
    protected $inputPath;
    /** @var string */
    protected $resultPath;

    /**
     * Should check is these variables could be processed
     * @param $a
     * @param $b
     * @return bool
     */
    protected abstract function validate($a, $b) :bool;

    /**
     * Should return result of operation
     * @param $a
     * @param $b
     * @return mixed
     */
    protected abstract function getResult($a, $b);

    /**
     * Should return operation name for logging purposes
     * @return string
     */
    protected abstract function getOperationName() :string;

    /**
     * AbstractAction constructor.
     * @param string $inputPath
     * @param string $resultPath
     * @param LoggerInterface $logger
     */
    public function __construct(string $inputPath, string $resultPath, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->inputPath = $inputPath;
        $this->resultPath = $resultPath;
    }

    /**
     * Decorator for logger trait
     * @param $level
     * @param $message
     * @param array $context
     */
    public function log($level, $message, array $context = array())
    {
        $this->logger->log($level, $message, $context);
    }

    /**
     * Main method of execution
     * @throws Exception
     */
    public function execute()
    {
        $this->notice('Started ' . $this->getOperationName() . ' operation');

        $fp = $this->getInputPointer();
        $this->checkOutput();

        while (($data = fgetcsv($fp, 1000, ";")) !== FALSE) {
            if($this->validate($data[0], $data[1])) {
                $result = $this->getResult($data[0], $data[1]);
                file_put_contents($this->resultPath, "${data[0]};${data[1]};${result}" . PHP_EOL, FILE_APPEND);
            } else {
                $this->notice("Numbers ${data[0]} and ${data[1]} are wrong");
            }
        }
        fclose($fp);

        $this->notice('Finished ' . $this->getOperationName() . ' operation');
    }

    /**
     * @return resource
     * @throws Exception
     */
    protected function getInputPointer()
    {
        $fp = fopen($this->inputPath, 'r');
        if(!$fp) throw new Exception("File cannot be opened");
        return $fp;
    }

    /**
     * flush previous results and creates new file
     */
    protected function checkOutput()
    {
        if(file_exists($this->resultPath)) {
            unlink($this->resultPath);
        }
        touch($this->resultPath);
    }
}