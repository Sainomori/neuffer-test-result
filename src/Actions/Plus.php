<?php
namespace Actions;


class Plus extends AbstractAction
{
    /**
     * @param $a
     * @param $b
     * @return bool
     */
    protected function validate($a, $b) :bool
    {
        if($a < 0 && $b < 0) return false;
        if($a < 0 && (abs($a) > $b)) return false;
        if($b < 0 && (abs($b) > $a)) return false;
        return true;
    }

    /**
     * @param $a
     * @param $b
     * @return mixed
     */
    protected function getResult($a, $b)
    {
        return intval($a) + intval($b);
    }

    /**
     * @return string
     */
    protected function getOperationName(): string
    {
        return 'plus';
    }
}