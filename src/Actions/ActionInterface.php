<?php


namespace Actions;


use Log\LoggerInterface;

interface ActionInterface
{
    public function __construct(string $inputPath, string $outputPath, LoggerInterface $logger);

    public function execute();
}