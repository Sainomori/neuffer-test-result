<?php
/**
 * I have so many thoughts if I should use my own autoloader or provided by composer.
 * Composer's one is better, but we have so little project... So here we are!
 */
spl_autoload_register(function ($class) {
    $file = __DIR__ . '/src/' . str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
    if (file_exists($file)) {
        require $file;
    }
});